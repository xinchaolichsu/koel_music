# koel [![Build Status](https://travis-ci.org/phanan/koel.svg?branch=master)](https://travis-ci.org/phanan/koel) [![OpenCollective](https://opencollective.com/koel/backers/badge.svg)](#backers) [![OpenCollective](https://opencollective.com/koel/sponsors/badge.svg)](#sponsors)

![Showcase](http://koel.phanan.net/dist/img/showcase.png?2)

## Intro

**Koel** (also stylized as **koel**, with a lowercase k) is a simple web-based personal audio streaming service written in [Vue](http://vuejs.org/) on the client side and [Laravel](http://laravel.com/) on the server side. Targeting web developers, Koel embraces some of the more modern web technologies – flexbox, audio, and drag-and-drop API to name a few – to do its job.

## Install and Upgrade Guide

For system requirements, installation guides, and troubleshooting, head over to [Wiki](https://github.com/phanan/koel/wiki).

If you are upgrading, see [Releases](https://github.com/phanan/koel/releases) for guides corresponding to your version.

- composer install
- npm install
- php artisan koel:init
- gulp
- npm run rebuild
- setting for media path: /var/www/html/...
- php artisan koel:sync

## Native App

An [electron](http://electron.atom.io/)-based desktop client, in its early days, is also available [here](https://github.com/phanan/koel-app).

